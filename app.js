// app.js
import storage from './core/storage';
import {Constant} from './core/constant';
App({
  onLaunch() {
    if(storage.get(Constant.USER_ID)) {
      this.globalData.userId = storage.get(Constant.USER_ID);
      this.globalData.userInfo = JSON.parse(storage.get(Constant.USER_ID));
    }
    // 登录
    // wx.login({
    //   success: res => {
    //     // 发送 res.code 到后台换取 openId, sessionKey, unionId
    //   }
    // })
    // 获取系统状态栏信息
    wx.getSystemInfo({
      success: e => {
        this.globalData.StatusBar = e.statusBarHeight;
        let capsule = wx.getMenuButtonBoundingClientRect();
        if (capsule) {
          this.globalData.Custom = capsule;
          this.globalData.CustomBar = capsule.bottom + capsule.top - e.statusBarHeight;
        } else {
          this.globalData.CustomBar = e.statusBarHeight + 50;
        }
      }
    })
  },
  globalData: {
    userInfo: null,
    userId: null
  },

})
