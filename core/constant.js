
export class Constant {
    static OPEN_ID = 'openid';
    static SESSION_CODE = 'session_code';
    static SESSION_KEY = 'session_key';
    static USER_MOBILE = 'mobile';
    static ACCESS_TOKEN = 'access_token';
    static USER_INFO = 'user_info';
    static PARENT_ID = 'parent_id';
    static USER_ID = 'user_id';
    static USER_INTEREST = 'user_interest';
    // static SERVICE_DOMAIN = 'https://k.ikeju.net/';
    static SERVICE_DOMAIN = 'https://3j765p2585.zicp.vip/';
    // static SERVICE_DOMAIN = 'http://192.168.31.103/';
    static SOCKET_DOMAIN = 'wss://admin.ikeju.net/';
    // static SOCKET_DOMAIN = 'wss://192.168.31.103:80/';
}