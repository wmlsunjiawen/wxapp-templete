
class StorageProvider {
  set(key, value) {
    wx.setStorageSync(key, value)
  }

  get(key) {
    return wx.getStorageSync(key)
  }

  remove(key) {
    wx.removeStorageSync(key)
  }

  clear() {
    wx.clearStorageSync()
  }
}

const storage = new StorageProvider();
export default storage;