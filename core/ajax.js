    import storage from './storage';
    import { Constant } from './constant';

    const domain = Constant.SERVICE_DOMAIN;
    class Ajax {
        isRefreshing = false;
        isTurnLogin = false;

        ajax(method, url, header, data, tokenRequired) {
            const that = this;
            return new Promise((resolve, reject)  => {
                method = method === 'postForm' ? 'post' : method;
                wx.request({ 
                    method: method,
                    url: url,
                    header: header,
                    data: data,
                    success: res => {
                        // console.log(res);
                        const response = new HttpResponse();
                        response.status = res.statusCode;
                        response.data = res.data;
                        response.headers = res.header;
                        if (response.status == 200 && response.data.status != 202) {
                            resolve(response);
                        } else if(response.data.status == 202){
                            wx.hideLoading({})
                            // 防止多次跳转
                            if(that.isTurnLogin) return;
                            that.isTurnLogin = true;
                            storage.remove(Constant.ACCESS_TOKEN)
                            storage.remove(Constant.SESSION_CODE)
                            storage.remove(Constant.USER_MOBILE) 
                            wx.reLaunch({
                                url: `/pages/login/login`
                            })
                        }
                        else if(response.data.status == '205'){
                            reject(response.data);
                            wx.hideLoading({})
                        } else{
                            reject(response.data);
                            wx.hideLoading({})
                        }
                    },
                    fail: err => {
                        wx.hideLoading();
                        wx.showToast({
                          title: '网络错误',
                          icon: 'none',
                        })
                    }
                })
            })
        }
        get(url, params, header, tokenRequired = true) {
            const tempHeader = header ? header : {};
            if (tokenRequired) {
                tempHeader['userid'] = +storage.get(Constant.USER_ID)
                tempHeader['authorization'] = storage.get(Constant.ACCESS_TOKEN);
            }
            // if (this.serialize(params)) {
            //   url += '?' + this.serialize(params);
            // }
            return this.ajax('get', url, tempHeader, params, tokenRequired);
        }
        post(url, data, header, tokenRequired = true) {
            const tempHeader = header ? header : {};
            if (tokenRequired) {
                tempHeader['userid'] = +storage.get(Constant.USER_ID)
                tempHeader['authorization'] = storage.get(Constant.ACCESS_TOKEN);
            }
            
            return this.ajax('post', url, tempHeader, data, tokenRequired)
        }

        postForm(url, data, header, tokenRequired = true) {
            const tempHeader = header ? header : {}
            if (tokenRequired) {
                tempHeader['authorization'] = storage.get(Constant.ACCESS_TOKEN)
                tempHeader['userid'] = +storage.get(Constant.USER_ID)
            }
        
            tempHeader['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8'
            return this.ajax('postForm', url, tempHeader, data, tokenRequired)
        }

        put(url, data, header, tokenRequired = true) {
            const tempHeader = header ? header : {};
            if (tokenRequired) {
                tempHeader['userid'] = +storage.get(Constant.USER_ID)
                tempHeader['authorization'] = storage.get(Constant.ACCESS_TOKEN);
            }
            return this.ajax('put', url, tempHeader, data, tokenRequired);
        }
        patch(url, data, header, tokenRequired = true) {
            const tempHeader = header ? header : {};
            if (tokenRequired) {
                tempHeader['userid'] = +storage.get(Constant.USER_ID)
                tempHeader['authorization'] = storage.get(Constant.ACCESS_TOKEN);
            }
            return this.ajax('patch', url, tempHeader, data, tokenRequired);
        }
        delete(url, data,header, tokenRequired = true) {
            const tempHeader = header ? header : {};
            if (tokenRequired) {
                tempHeader['userid'] = +storage.get(Constant.USER_ID)
                tempHeader['authorization'] = storage.get(Constant.ACCESS_TOKEN);
            }
            return this.ajax('delete', url, tempHeader, data, tokenRequired);
        }
    }
    export class HttpResponse {
        data;
        headers;
        status;
    }

    /*返回结果处理*/
    export class LinkResponse {

        results;
        totalPages;

        constructor(httpResponse) {
            const results = httpResponse.data;
            this.results = results;
            if(httpResponse.pages) this.totalPages = httpResponse.pages;
        }

        // static generateEntityData(results);
    }
    const ajax = new Ajax();
    export default ajax;