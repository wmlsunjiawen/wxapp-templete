

export default class Tools {
  /**
   * 校验是否有效邮箱
   * @param email 手机号码
   */
  static isEmail(email) {
    if(email) {
      const reg = /^[A-Za-z0-9]+([_\.][A-Za-z0-9]+)*@([A-Za-z0-9\-]+\.)+[A-Za-z]{2,6}$/;
      return reg.test(email);
    }
    return false;
  }
  /**
   * 校验是否是有效手机号码
   * @param phoneNumber 手机号码
   */
  static isPhoneNumber(phoneNumber) {
    if (phoneNumber) {
      const reg = /^(1[3-9])\d{9}$/g;
      return reg.test(phoneNumber);
    }
    return true;
  }

  static isPlateNumber(plateNumber) {
    if (plateNumber) {
      const rule1 = /^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼][A-Z](?![A-HJ-NP-Z]{5})[A-HJ-NP-Z\d]{5}$/;
      const rule2 = /^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼][A-Z](?![A-HJ-NP-Z]{4})[A-HJ-NP-Z\d]{4}学$/;
      const rule3 = /^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼][A-Z](?![A-HJ-NP-Z]{4})[A-HJ-NP-Z\d]{4}警$/;
      const rule4 = /^[A-Z]{2}(?![A-HJ-NP-Z]{5})[A-HJ-NP-Z\d]{5}$/;
      const rule5 = /^WJ[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼]{0,1}\d{4,5}[A-H_J-NP-Z\d]$/;
      const rule6 = /^粤[A-HJ-NP-Z\d][A-HJ-NP-Z\d]{4}港|澳$/;
      const rule7 = /^\d{6}使$/;
      const rule8 = /^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼][A-Z](?![A-HJ-NP-Z]{4})[A-HJ-NP-Z\d]{4}领$/;
      const rule9 = /^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼][A-Z](?![A-HJ-NP-Z]{4})[A-HJ-NP-Z\d]{4}挂$/;
      const rule10 = /^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼][A-Z][D|F][A-HJ-NP-Z\d]\d{4}$/;
      const rule11 = /^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼][A-Z]\d{5}[D|F]$/;

      const str = plateNumber.toUpperCase();
      return rule1.test(str) || rule2.test(str) || rule3.test(str) || rule4.test(str)
        || rule5.test(str) || rule6.test(str) || rule7.test(str)
        || rule8.test(str) || rule9.test(str) || rule10.test(str) || rule11.test(str);
    }
    return true;
  }

  // 格式化日期
  static formatDate (timestamp, format) {
      formatStr = function(n) {
        if (n < 10) {
            return '0' + n;
        }
        return n;
      }
      if (!timestamp) {
        return '--';
      }
      var date = new Date(timestamp);
      var year = date.getFullYear();
      var month = date.getMonth() + 1;
      var day = date.getDate();

      var hour = date.getHours();
      var minute = date.getMinutes();
      var second = date.getSeconds();
      if (format === 'yyyy/MM/dd') {
        return year + '/' + formatStr(month) + '/' + formatStr(day);
      }
      if (format === 'yyyy-MM-dd') {
        return year + '-' + formatStr(month) + '-' + formatStr(day);
      }
      if (format === 'yyyy.MM.dd') {
        return year + '.' + formatStr(month) + '.' + formatStr(day);
      }
      if (format === 'hh:mm') {
        return formatStr(hour) + ':' + formatStr(minute);
      }
      if (format === 'hh:mm:ss') {
        return formatStr(hour) + ':' + formatStr(minute) + ':' + formatStr(second);
      }
      if (format === 'yyyy/MM/dd hh:mm:ss') {
        return year + '/' + formatStr(month) + '/' + formatStr(day) + ' ' + formatStr(hour) + ':' + formatStr(minute) + ':' + formatStr(second);
      }
      if (format === 'yyyy-MM-dd hh:mm') {
        return year + '-' + formatStr(month) + '-' + formatStr(day) + ' ' + formatStr(hour) + ':' + formatStr(minute);
      }
      if (format === 'yyyy.MM.dd hh:mm') {
        return year + '.' + formatStr(month) + '.' + formatStr(day) + ' ' + formatStr(hour) + ':' + formatStr(minute);
      }
      if(format === 'yyyy年MM月dd日'){
        return year + '年' + formatStr(month) + '月' + formatStr(day) + '日';
      }
      if(format === 'dd'){
        return formatStr(day);
      }
    return year + '-' + formatStr(month) + '-' + formatStr(day) + ' ' + formatStr(hour) + ':' + formatStr(minute) + ':' + formatStr(second);
  }

  // 格式化时间
  static formatTime (timestamp) {
    formatStr = function (n) {
      if (n < 10) {
        return '0' + n;
      }
      return n;
    }
    var time = Math.floor(timestamp / 1000)
    var second = time % 60
    var minute = Math.floor(time / 60) % 60
    var hour = Math.floor(time / 3600)
    return formatStr(hour) + ':' + formatStr(minute) + ':' + formatStr(second)
  }

  static  countDownFormat (timestamp) {
    formatStr = function (n) {
      if (n < 10) {
        return '0' + n;
      }
      return n;
    }
    var time = Math.floor(timestamp / 1000)
    var second = time % 60
    var minute = Math.floor(time / 60) % 60
    var hour = Math.floor(time / 3600)
    return formatStr(hour) + ':' + formatStr(minute) + ':' + formatStr(second)
  }

  static formatOrdinal = function (ordinalStr) {
    let newOrdinal = '';   // 选项
    let swichFn = function(str) {
      let newStr = ''
      switch(str) {
        case 0:
          newStr = 'A';
          break;
        case 1:
          newStr = 'B';
          break;
        case 2:
          newStr = 'C';
          break;
        case 3:
          newStr = 'D';
          break;
        case 4:
          newStr = 'E';
          break;
        case 5:
          newStr = 'F';
          break;
        case 6:
          newStr = 'G';
          break;
        case 7:
          newStr = 'H';
          break;
      }
      return newStr;
    }
    newOrdinal = swichFn(ordinalStr)
    return newOrdinal;
  }
}