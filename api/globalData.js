import ajax from '../core/ajax';
// import * as Entity from '../../entity/entity';
import { Constant } from '../core/constant';

const domain = Constant.SERVICE_DOMAIN;

class GlobalApi {
    constructor () {};

    /**
     * 获取首页近期直播数据
     */
    static requestNearLiveData(userId) {
        return new Promise((resolve, reject) => {
            ajax.get(`${domain}mini_online_live.htm`,{userId}).then((res) => {
                resolve(res.data.lives);
            }).catch((err) => {
                reject(err);
            });
        });
    }
    /**
     * 获取首页免费好课数据
     */
    static requestFreeCourseData() {
        return new Promise((resolve, reject) => {
            ajax.get(`${domain}mini_recommend_course.htm`).then((res) => {
                resolve(res.data.courses);
            }).catch((err) => {
                reject(err);
            });
        });
    }
    /**
     * 获取首页推荐好课数据
     */
    static requestRecommendCourseData() {
        return new Promise((resolve, reject) => {
            ajax.get(`${domain}mini_sub_courses.htm`).then((res) => {
                resolve(res.data);
            }).catch((err) => {
                reject(err);
            });
        });
    }
    /**
     * 获取首页推荐资讯数据
     */
    static requestRecommendNewsData() {
        return new Promise((resolve, reject) => {
            ajax.get(`${domain}mini_recommend_info.htm`).then((res) => {
                resolve(res.data.informations);
            }).catch((err) => {
                reject(err);
            });;
        });
    }
    /**
     * 获取首页讲师列表数据
     */
    static requestLecuterData() {
        return new Promise((resolve, reject) => {
            ajax.get(`${domain}mini_recommend_lecuter.htm`).then((res) => {
                resolve(res.data.lecturers);
            }).catch((err) => {
                reject(err);
            });
        });
    }
    /**
     * 获取轮播图数据
     */
    static requestBannerData() {
        return new Promise((resolve, reject) => {
            ajax.get(`${domain}mini_banner_load.htm`).then((res) => {
                resolve(res.data.banners);
            }).catch((err) => {
                reject(err);
            });
        });
    }
    /**
     * 加载公共专业筛选分类
     */
    static requestSubjectTypeData() {
        return new Promise((resolve, reject) => {
            ajax.get(`${domain}mini_subject.htm`).then((res) => {
                resolve(res.data.subjects);
            }).catch((err) => {
                reject(err);
            });
        });
    }
    /**
     * 加载所以三级专业分类
     */
    static requestThreeSubjectData() {
        return new Promise((resolve, reject) => {
            ajax.get(`${domain}mini_three_subject.htm`).then((res) => {
                resolve(res.data.subjects);
            }).catch((err) => {
                reject(err);
            });
        });
    }
    /**
     * 用户收藏
     * id
     * type 收藏类型 0课程收藏 1话题收藏 2问答收藏
     * userId
     */
    static requestAddCollect(params) {
        return new Promise((resolve, reject) => {
            ajax.postForm(`${domain}mini_user_collect.htm`,params).then((res) => {
                resolve(res.data);
            }).catch((err) => {
                reject(err);
            });
        });
    }
    /**
     * 用户取消收藏
     * id
     * userId
     */
    static deleteCollect(params) {
        return new Promise((resolve, reject) => {
            ajax.delete(`${domain}mini_user_collect_delete.htm`,params).then((res) => {
                resolve(res.data);
            }).catch((err) => {
                reject(err);
            });
        });
    }
}
export default GlobalApi;