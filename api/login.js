    import ajax,{ HttpResponse } from '../core/ajax';
    // import * as Entity from '../../entity/entity';
    import { Constant } from '../core/constant';

    const domain = Constant.SERVICE_DOMAIN;

    class LoginApi {
        constructor () {};
        /**
         * code换取 登录
         */
        static submitCode(code,companyId ,mobile ) {
            return new Promise((resolve, reject) => {
                ajax.postForm(`${domain}mini_feishu_reg.htm`, {code,companyId ,mobile }, null,false).then((res) => {
                    resolve(res.data);
                    // observe.complete();
                }).catch((err) => {
                    reject(err);
                });
            });
        }
        /**
         * 验证手机号
         */
        static checkReMobile(mobile ) {
            return new Promise((resolve, reject) => {
                ajax.postForm(`${domain}mini_verify_remobile.htm`, {mobile }, null,false).then((res) => {
                    resolve(res.data.valid);
                }).catch((err) => {
                    reject(err);
                });
            });
        }

        /**
         * 验证用户账号是否重复
         */
        static checkReUsername(userName ) {
            return new Promise((resolve, reject) => {
                ajax.postForm(`${domain}mini_verify_username.htm`, {userName }, null,false).then((res) => {
                    resolve(res.data.valid);
                }).catch((err) => {
                    reject(err);
                });
            });
        }
        
        /**
         * 发送注册短信验证码
         */
        static sendRegisterCode(mobile ) {
            return new Promise((resolve, reject) => {
                ajax.get(`${domain}mini_code_sms_send_register.htm`, {mobile }, null,false).then((res) => {
                    resolve(res.data.ret);
                }).catch((err) => {
                    reject(err);
                });
            });
        }
        /**
         *  获取解析用户手机号
         */
        static requestUserMobileDecryptData(params ) {
            return new Promise((resolve, reject) => {
                ajax.postForm(`${domain}mini_getphone.htm`, params, null,false).then((res) => {
                    resolve(res.data);
                }).catch((err) => {
                    reject(err);
                });
            });
        }
        /**
         *  注册保存用户
         */
        static saveUserRegister(params ) {
            return new Promise((resolve, reject) => {
                ajax.postForm(`${domain}mini_register_save.htm`, params, null,false).then((res) => {
                    resolve(res.data);
                }).catch((err) => {
                    reject(err);
                });
            });
        }
        /**
         *  账号密码登录
         */
        static submitPasswordLogin(params ) {
            return new Promise((resolve, reject) => {
                ajax.postForm(`${domain}mini_verify_user_pws.htm`, params, null,false).then((res) => {
                    resolve(res.data);
                }).catch((err) => {
                    reject(err);
                });
            });
        }
        /**
         *  code 换取 openid
         */
        static submitCodeToOpenid(code ) {
            return new Promise((resolve, reject) => {
                ajax.postForm(`${domain}mini_getopenid.htm`, {code}, null,false).then((res) => {
                    resolve(res.data);
                }).catch((err) => {
                    reject(err);
                });
            });
        }
    }
    export default LoginApi;