// pages/open-bill/open-bill.js
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    CustomBar: app.globalData.CustomBar,
    isModalShow: false,  // 弹层开关
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  // 关闭弹层
  hideModal() {
    this.setData({
      isModalShow: false,
    })
  },

  // 打开弹层
  openModal(e) {
    this.setData({
      isModalShow: true
    })
  },

  // 跳转填写单据信息
  toAddOrder() {
    wx.navigateTo({
      url: `/pages/add-order/add-order`,
    })
  }


})