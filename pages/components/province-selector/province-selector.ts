// pages/components/province-selector.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    provinces: [
      ["京", "津", "冀", "晋", "蒙", "辽", "吉"],
      ["黑", "沪", "苏", "浙", "皖", "闽", "赣"],
      ["鲁", "豫", "鄂", "湘", "粤", "桂", "琼"],
      ["渝", "川", "贵", "云", "藏", "陕", "甘"],
      ["青", "宁", "新", "台", "WJ", '']
    ],
    isShowKeyBoard: false
  },

  /**
   * 组件的方法列表
   */
  methods: {
    showProvinces() {
      this.setData({
        isShowKeyBoard: true
      });
    },
    hideProvinces() {
      this.setData({
        isShowKeyBoard: false
      });
    },
    selectedProvince(event: any) {
      this.triggerEvent("inputProvince", event.currentTarget.dataset.column);
    }
  }
})
