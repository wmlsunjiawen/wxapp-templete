// pages/receive-car/receive-car.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  // 选择车牌照片
  chooseImage() {
    wx.chooseImage({
      success: function(res) {
        console.log(res);

      },
    })
  },
  // 跳转手动输入车牌
  toInputCarNum() {
    wx.navigateTo({
      url: `/pages/manual-input-carNum/manual-input-carNum`,
    })
  },

  // 拉取相机拍照


})