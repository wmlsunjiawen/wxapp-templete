// pages/manual-input-carNum/manual-input-carNum.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    carNum: null,  // 车牌号
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  // 用户输入车牌号
  inputCarNum(e) {
    
    this.setData({
      carNum: e.detail.value.trim()
    })

  }
})