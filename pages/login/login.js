// pages/login/login.js
// import storage from '../../core/storage'
// import {Constant} from '../../core/constant'
// import loginApi from '../../api/login'
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    username: '',
    password: '',

  },

  /**
   * 生命周期函数--监听页面加载
   */
   onLoad: function (options) {
    const that = this;
    // code 换取 openid
    if(!storage.get(Constant.SESSION_KEY)) {
      wx.checkSession({
        success: async (suc) => {
          let result = await that.submitCodeToOpenid(storage.get(Constant.SESSION_CODE))
          storage.set(Constant.OPEN_ID, result.openid)
          storage.set(Constant.ACCESS_TOKEN, result.access_token)
          storage.set(Constant.SESSION_KEY, result.session_key)
        },
        fail(err) {
          wx.login({
            success: async (response) => {
              storage.set(Constant.SESSION_CODE,response.code);
              let result = await that.submitCodeToOpenid(response.code)
              storage.set(Constant.OPEN_ID, result.openid)
              storage.set(Constant.ACCESS_TOKEN, result.access_token)
              storage.set(Constant.SESSION_KEY, result.session_key)
            }
          });
        }
      })
    }
  },
  // code 换 openid
  submitCodeToOpenid(code) {
    return loginApi.submitCodeToOpenid(code)
  },
  // 跳转注册页面
  toRegisterPage() {
    wx.navigateTo({
      url: '/pages/register/register',
    })
  },
  // 拉取手机号
  async getPhoneNumber(res){
    console.log(res,'getPhone')
    const {type} = res.currentTarget.dataset;

    const that = this;
    // 用户同意授权
    if(res.detail.errMsg.indexOf('ok')>=0){
      wx.checkSession({
        success(suc) {
          // console.log(suc)
          that.decryptInfo(res, storage.get(Constant.SESSION_KEY),type);
        },
        fail(err) {
          wx.login({
            success: async (response) => {
              storage.set(Constant.SESSION_CODE,response.code);
              let result = await that.submitCodeToOpenid(response.code)
              storage.set(Constant.OPEN_ID, result.openid)
              storage.set(Constant.ACCESS_TOKEN, result.access_token)
              storage.set(Constant.SESSION_KEY, result.session_key)
              that.decryptInfo(res, result.session_key, type);
            }
          });
        }
      })

    }else{
      wx.showModal({
        title: '提示',
        content: '需要授权手机号用于后续绑定您的账号信息',
        showCancel:false,
        confirmText: '返回授权',
      })
    }
  },  
  // 解密用户手机数据
  decryptInfo(encryptedData,session_key, type){
    wx.showLoading({title: '登录中...'})
    const that = this;
    const param = {};
    param.encryptedData = encryptedData.detail.encryptedData;
    param.iv = encryptedData.detail.iv;
    param.session_key = session_key;
    param.openid = storage.get(Constant.OPEN_ID);
    param.access_token = storage.get(Constant.ACCESS_TOKEN)
    param.type = type;   // 1，注册， 2登录
    loginApi.requestUserMobileDecryptData(param).then((res) => {
      console.log(res);
      wx.hideLoading({});
      // 判断登录是否成功
      if(res.status == 202) {
        wx.showModal({
          title: '提示',
          content: '登录失败，请重试',
          showCancel: false,
        })
        return
      }else {
        storage.set(Constant.USER_MOBILE, res.mobile )
        // 判断用户点击的按钮为注册还是一键登录  1 注册， 2登录
        if(type == 1) {
          // 未注册用户跳转注册页面， 已注册用户提示已注册跳转首页
          if(res.user != null) {
            // 缓存用户信息
            storage.set(Constant.USER_INFO, JSON.stringify(res.user));
            storage.set(Constant.USER_ID, JSON.stringify(res.user.id));
            app.globalData.userId = res.user.id;
            app.globalData.userInfo = res.user;

            wx.showModal({
              title: '提示',
              content: '该手机号已注册，可点击确认直接登录',
              confirmText: '确认',
              showCancel: false,
              success : () => {
                that.turnToIndexPage();
              }
            })
          }else{
            this.toRegisterPage();
          }
        }else {
          // 微信手机号直接登录
          storage.set(Constant.USER_MOBILE, res.mobile )
          storage.set(Constant.USER_INFO, JSON.stringify(res.user));
          storage.set(Constant.USER_ID, JSON.stringify(res.user.id));
          app.globalData.userId = res.user.id;
          app.globalData.userInfo = res.user;
          this.turnToIndexPage();
        }
      }

    },(err) => {
      console.log(err);
      wx.showModal({
        title: '提示',
        contnet: '拉取手机号失败，请重试',
        showCancel:false,
      })
    });
  },

  // 账号密码登录
  submitLogin() {
    const username = this.data.username,
          password = this.data.password;
    if(!username || !password) {
      wx.showToast({
        title: '请填写账号密码',
      })
      return;
    }
    const params = {
      username,
      password,
      openid : storage.get(Constant.OPEN_ID),
      access_token  : storage.get(Constant.ACCESS_TOKEN),
    }
    wx.showLoading({title: '登录中...'});
    loginApi.submitPasswordLogin(params).then((res) => {
      console.log(res);
      wx.hideLoading();
      if(res.valid) {
        storage.set(Constant.USER_INFO,JSON.stringify(res.user))
        storage.set(Constant.USER_MOBILE,res.user.mobile)
        storage.set(Constant.USER_ID,res.user.id)
        app.globalData.userId = res.user.id;
        app.globalData.userInfo = res.user;
        this.turnToIndexPage()
      }else {
        wx.showToast({
          title: '账号密码不正确',
        })
      }

    })
  },

  // input输入
  inputChange(e) {
    const {type } = e.currentTarget.dataset;

    switch(type) {
      case 'username' :
        this.setData({
          username: e.detail.value.trim()
        })
        break;
      case 'password' :
        this.setData({
          password: e.detail.value.trim()
        })
        break;
    }
  },

  // 跳转主页
  turnToIndexPage() {
    const pages = getCurrentPages(),
    prevPage = pages[pages.length - 2]
    prevPage.onLoad();
    wx.navigateBack({})
  },

})