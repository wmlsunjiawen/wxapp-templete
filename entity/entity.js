
import storage from '../core/storage';
import { Constant } from '../core/constant';

export class RequestCertificateListParams {
    userId = +storage.get(Constant.USER_ID); // 用户id
    parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
    pageNumber = 0; // 当前页码
    pageSize = 12; // 每页条数
    sortName = ''; // 排序字段名
    sortOrder = ''; // 排序方式 asc或desc
}

export class CheckEmailIsRepeatParams {
    userId = +storage.get(Constant.USER_ID); // 用户id
    parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
    email; // 邮箱
}

export class UpdateUserMsgParams {
    userId = +storage.get(Constant.USER_ID); // 用户id
    parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
    sex; // 性别（1为男、0为女、-1为保密 ）
    trueName = ''; // 真实姓名
    email = ''; // 邮箱
}

export class RequestCurrentExamListParams {
    userId = +storage.get(Constant.USER_ID); // 用户id
    parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
    pageNumber = 0; // 当前页码
    pageSize = 12; // 每页条数
    sortName = ''; // 排序字段名
    sortOrder = ''; // 排序方式 asc或desc
}

export class RequestWrongTopicListParams {
    userId = +storage.get(Constant.USER_ID); // 用户id
    parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
    pageNumber = 0; // 当前页码
    pageSize = 12; // 每页条数
    sortName = ''; // 排序字段名
    sortOrder = ''; // 排序方式 asc或desc
}
export class RequestHistoryExamListParams {
    userId = +storage.get(Constant.USER_ID); // 用户id
    parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
    pageNumber = 0; // 当前页码
    pageSize = 12; // 每页条数
    sortName = ''; // 排序字段名
    sortOrder = ''; // 排序方式 asc或desc
}
export class RequestCourseListParams {
    userId = +storage.get(Constant.USER_ID); // 用户id
    parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
    pageNumber = 0; // 当前页码
    pageSize = 12; // 每页条数
    sortName = ''; // 排序字段名
    sortOrder = ''; // 排序方式 asc或desc
}

export class RequestCourseDetailParams {
    userId = +storage.get(Constant.USER_ID); // 用户id
    parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
    id; // 对应课程id
    ccw_id = ''; // UserCCW对应id，记录考生学习课程时间和完成率
}
export class RequestCoursePracticeParams {
    userId = +storage.get(Constant.USER_ID); // 用户id
    parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
    ccwp_id = ''; //  UserCCW对应id，记录考生学习课程时间和完成率
}
export class SaveStudyProgressParams {
    userId = +storage.get(Constant.USER_ID); // 用户id
    parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
    ccw_id = ''; //  CourseCW，
    uccw_id = ''; // UserCCW对应id   首次保存进度为空，
    course_id = ''; // 课程id
    studySecond; // （当前学习时间，单位为 秒）
    totalSecond; // （本课件完成学习总时间，单位为 秒）
}
export class RequestDownloadUrlParams {
    userId = +storage.get(Constant.USER_ID); // 用户id
    parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
    ccw_id = ''; //  UserCCW对应id，记录考生学习课程时间和完成率
    id = '';  // 课程对应id
}
export class SubmitPracticeAnswerParams {
    userId = +storage.get(Constant.USER_ID); // 用户id
    parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
    ccweq_id = ''; //   练习试题id
    value = '';  // 考试试题答案, （1）单选题取ExamDetailQuestionItemsMsgEntity的id（2）多选题将答案id用半角逗号拼接（3）填空题，将各填空答案用@ikeju@ 字符串连接（4）判断题，提交对应答案 1为正确 0为错误（5）简答题 ，提交答案全文
}
export class ExamResultParams {
    userId = +storage.get(Constant.USER_ID); // 用户id
    parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
    id = ''; //   考试id，UseExam的id
}
export class RequestWrongTopicDetailParams {
    userId = +storage.get(Constant.USER_ID); // 用户id
    parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
    ue_id = ''; //   对应UserExam的id
}



export class ExamDetailQuestionTypeEntity  {
  id = ''; // 
  utpi_tpi_idnumber = ''; // 
  utpi_ue_idnumber = ''; // 
  utpiqs = []; // 所有小题集合
  tpiany = ''; // 类型大题信息
  addTimenumber = ''; 
  createTrueNamestring = ''; 
  createUserNamestring = ''; 
  createUserIdnumber = ''; 
  deleteStatusnumber = ''; 
  trueNamestring = ''; 
  userNamestring = ''; 
  userIdnumber = ''; 
}

export class ExamDetailEntity {
  camera_times = []; // 随机弹出人脸识别时间点数组
  errmsg = ''; // 错误信息n
  obj = new ExamDetailMsgEntity(); // 考试信息
  puser = {}; // 用户公司信息 
  tp = new ExamDetailTestPaperMsgEntity(); // 试卷信息
  ue = new ExamDetailUserTestPaperEntity(); // 考生试卷信息
  utpis = []; // 考题数组 ExamDetailQuestionTypeEntity
  status; // 网络状态
  sdkAppId; // 监控sdk appid
  userSig; // 监控sdk 签名
  userDefineRecordId; // 

}

export class ExamDetailMsgEntity {
  exam_answer_count = 0; // 允许答题次数，0为不限次数
  exam_begin_date; // 考试开始时间
  exam_camera; // 是否启用摄像头，0为不启用，1为启用
  exam_cat_id; // 考试分类id
  exam_cat_name; // 考试分类名
  exam_comment; // 考试评语，json格式，不同分数区间不同评语
  exam_comment_status; // 考试评语状态，0为关闭，1为开启
  exam_duration = 0; // 单位：分钟，考试时长，0为不限时长
  exam_end_date; // 考试结束时间
  exam_face_rec; // 是否启用人脸识别，exam_login_type必须为0才有效，需要上传人脸照片，对比成功后才可以进入考试，1为启用
  exam_fee; // 单位：元，考试付费金额
  exam_fee_status; // 是否为付费考试，0为否，1为是
  exam_integral; // 积分规则json数据
  exam_integral_status; // 考试积分状态,0为关闭，1为开启
  exam_intro; // 考试说明
  exam_min_time = 0; // 单位：秒，最短答题时长，大于0为有效值，小于等于0不限制
  exam_misoperation = 0; // 单位：秒， 无操作对应时间（秒）后自动交卷,0为不限制
  exam_mode; // 考试模式，默认0为考试，1为练习（练习模式可以随时查看答案），2为竞赛模式
  exam_name; // 考试名称
  exam_pass_score; // 及格分数
  exam_public_comment; // 考试结束语，exam_comment不配置的时候显示选项
  exam_rank; // 是否显示考试前十的名次,0为显示，1为不显示
  exam_result; // 是否交卷后显示成绩，0为显示，1为不显示
  exam_status; // 考试状态，0为开启，1为禁止
  exam_switch_page = 0; // 单位：次， 切换页面几次自动交卷,0为不限制
  exam_total_duration; //  单位：秒，当试卷类型为单题限时时候该字段生效，该字段记录试卷整体时间=单题限时之和
  exam_total_score; // 考试总分数，等于试卷总分数
  exam_tp_id; // 考试对应的试卷id
  exam_water; //  是否启用试题水印,0为禁用，1为启用
  exam_wrong_count = 0; // 允许错题次数，用在竞赛中，0为不限次数，大于0为限制对应次数
  userId; // 用户id
  exam_face_error_submit; // 0为记录识别失败仅仅给出提示，1为人脸识别失败给出提示后直接交卷；
  userName; // 用户名，该字段唯一
  exam_monitor = 0;   // 是否开启监考中心,0为不开启，1为开启
  createTrueName; 
  createUserId; 
  createUserName; 
  deleteStatus; 
  ecis; 
  exam_attend_rate; 
  exam_depart_names; 
  exam_departs; 
  exam_highestScore; 
  exam_login_password; 
  exam_minimumScore; 
  exam_persons; 
  exam_qr; 
  exam_role; 
  exam_should_count; 
  exam_top; 
  exam_users; 
  exam_visit_code; 
  id; 
  tp;  // 试卷信息
  trueName; 
  exam_login_type; // 考试登录方式，0为账号密码登录，1为免登录考试（需要录入指定信息）暂时不用
  
}
export class ExamDetailTestPaperMsgEntity {
  id; // 
  tp_cat_id; // 试卷分类id
  tp_cat_name; // 试卷分类名称
  tp_extract_status; // 抽题组卷的状态，0为未抽题，1为完成抽题
  tp_mode; // 组卷方式，默认为0：随机组卷，1：选题组卷，2：抽题组卷
  tp_name; // 试卷名称
  tp_score; // 试卷总分
  tp_time_mode; // 时长计算模式，默认为0：整体计算，1：单题计算
  tp_total_count; // 试卷总题数
  addTime; 
  createTrueName; 
  createUserId; 
  createUserName; 
  deleteStatus; 
  trueName; 
  userId; 
  userName; 

}
export class ExamDetailUserTestPaperEntity {
  id; // id
  ue_exam_begin_date; // 考试开始时间
  ue_exam_duration; // 考试时长=交卷时间-开始时间，如果多次退出，时长=考试结束时间-考试开始时
  ue_exam_end_date = 0; // 考试结束时间,考试过程中考生可以退出考试，再次进入考试时如果超过该时间，则当前试卷已经无效，系统会根据考试设置确定是否能继续生成一张试卷或不能继续考试
  ue_exam_id; // 对应考试（Exam）id
  ue_exam_intro; // 对应考试说明
  ue_face_status; // 是否已经完成人脸识别，一场考试只扣除一个人脸识别次数，第一次识别后该字段值为1
  ue_fee; // 是否付费考试,0为免费考试，1为付费考试
  ue_fee_status; // 是否已经付费,0为未付费，1为已付费
  ue_pass; // 考试是否通过，0为不通过，1为通过
  ue_retest; // 是否补考,0为正常考试，1为补考
  ue_begin_num = 0; // 用在单题计时模式，答题中途重新进入直接定位该题，不可以答已经作答的题目
  addTime; 
  createTrueName; 
  createUserId; 
  createUserName; 
  deleteStatus; 
  departName; 
  exam; 
  trueName; 
  ue_answer_count; 
  ue_exam_correct; 
  ue_exam_status; 
  ue_exam_submit_date; 
  ue_score; 
  ue_right_count; 
  ue_status; 
  ue_type; 
  ue_wrong_count = 0; 
  userId; 
  userName; 
  ue_exam_name;
  utpiqs = [];  // ExamDetailQuestionTypeEntity
}
export class ExamDetailQuestionItemsEntity {
  id; // 
  eq = new ExamDetailQuestionItemsEQEntity(); // 
  tpiq  = new ExamDetailQuestionItemsMsgEntity(); // 考题信息
  utpiq_answer = ''; // 答题答案
  utpiq_error; // 是否错题，用在错题集
  utpiq_mark = 0; // 是否做了标记，0为没做，1为做标记
  utpiq_result; // 该题的回答结果，0为错误，1为正确
  utpiq_score; // 该题得分
  tpi_total_count; // 大题题数
  tpi_total_score; // 大题总分
  tpi_title = '';  // 大题题型
  user_answer = ''; // 考生答案，用于问卷展示
  true_answer = ''; // 正确答案，用于问卷展示
  isShow = false; // 是否显示错题集答案解析
  textareaShow = false; // textarea文本框是否显示  简答题
  index; // 下标
  utpiq_tpiq_id; // 
  utpiq_ue_id; // 
  utpiq_utpi_id; // 
  addTime; 
  createTrueName; 
  createUserName; 
  trueName; 
  userName; 
  createUserId; 
  deleteStatus; 
  userId; 
  childs = []; // 组合试题，子试题数组
  recordlist = []; //  录音试题，录音数组  ExamDetailSoundRecordEntity
}
export class ExamDetailSoundRecordEntity {
  id; // 
  url; // 录音音频路径 
}
export class ExamDetailQuestionItemsOptionEntity {
  id; // 
  eq_id; // 对应的试题id
  eqi_answer; // 该内容项目是否为选择题答案，0为不是，1为是
  eqi_content = ''; // 选项内容
  eqi_ordinal; // 选项序号
  fill_answer; // 填空题答案
  checked; // 是否选中
  textareaShow = false; // textarea文本框是否显示   // 填空题
  addTime; 
  createUserId; 
  deleteStatus; 
  userId; 
  createTrueName;
  createUserName;
  userName;
  trueName;
  eq; // 
}

export class CreateExamTestPaperParams {
  userId = +storage.get(Constant.USER_ID); // 用户id
  parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
  exam_id = 0; // 当前考试id
}
export class CheckIDCardParams {
  userId = +storage.get(Constant.USER_ID); // 用户id
  parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
  exam_id = 0; // 当前考试id
}
export class RequestExamDetailParams {
  userId = +storage.get(Constant.USER_ID); // 用户id
  parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
  examId = 0; // 当前考试id
}
export class DeleteWrongTopicParams {
  userId = +storage.get(Constant.USER_ID); // 用户id
  parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
  utpiq_id = 0; // UserTestPaperItemQuestion的id 试题id
}

export class SaveTestPaperParams {
  userId = +storage.get(Constant.USER_ID); // 用户id
  parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
  user_exam_id = 0; // 考试id  UseExam的id 
}

export class SubmitExamAnswerParams {
  userId = +storage.get(Constant.USER_ID); // 用户id
  parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
  utpiq_id = 0; // 考试试题id，ExamDetailQuestionItemsMsgEntity
  eqi_id = 0; // 考试试题答案, （1）单选题取ExamDetailQuestionItemsMsgEntity的id（2）多选题将答案id用半角逗号拼接（3）填空题，将各填空答案用@ikeju@ 字符串连接（4）判断题，提交对应答案 1为正确 0为错误（5）简答题 ，提交答案全文
}

export class TestQuestionRemarkParams {
  userId = +storage.get(Constant.USER_ID); // 用户id
  parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
  utpiq_id = 0; // 考试试题id，ExamDetailQuestionItemsMsgEntity
}

export class UploadFileFaceImgParams {
  userId = +storage.get(Constant.USER_ID); // 用户id
  parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
  ue_id = 0; // 考试id，UseExam的id
  time; // 
}

export class SaveChangeBackgroundParams {
  userId = +storage.get(Constant.USER_ID); // 用户id
  parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
  ue_id = 0; // 考试id，UseExam的id
}
export class SaveOnOperateStatusParams {
  userId = +storage.get(Constant.USER_ID); // 用户id
  parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
  ue_id = 0; // 考试id，UseExam的id
}

export class SaveSingleExamProcessParams {
  userId = +storage.get(Constant.USER_ID); // 用户id
  parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
  ue_id = 0; // 考试id，UseExam的id
}

export class PublicRequestParams {
  userId = +storage.get(Constant.USER_ID); // 用户id
  parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
}

export class RequestMatchScoreParams {
  userId = +storage.get(Constant.USER_ID); // 用户id
  parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
  utpiq_id = 0; // （考试试题id，UserTestPaperItemQuestion的id）
  user_exam_id;   // 考生考试id  UseExam的id
}

export class SubmitRecordParams {
  userId = +storage.get(Constant.USER_ID); // 用户id
  parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
  utpiq_id = 0; // 考试试题id，ExamDetailQuestionItemsMsgEntity
}

export class DeleteRecordParams {
  userId = +storage.get(Constant.USER_ID); // 用户id
  parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
  utpiq_id = 0; // 考试试题id，ExamDetailQuestionItemsMsgEntity
  url = '';  // 录音地址
}

export class PayExamOrderParams {
  userId = +storage.get(Constant.USER_ID); // 用户id
  parentId = +storage.get(Constant.PARENT_ID); // 用户父级id 
  exam_id = 0; // 考试id
  ue_id;  // 用户考试id 
}











